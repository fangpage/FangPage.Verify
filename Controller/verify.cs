﻿using System;
using System.Collections.Generic;
using System.Text;
using FangPage.Core;

namespace FangPage.Verify.Controller
{
    /// <summary>
    /// 验证码图片
    /// </summary>
    public class verify : FPController
    {
        public override void Controller()
        {
            //生成验证码
            FPVerify validateCode = new FPVerify();
            string code = validateCode.CreateValidateCode(4);
            FPSession.Set("FP_VERIFY", code);
            byte[] bytes = validateCode.CreateValidateGraphic(code);
            //输入出图片
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            FPContext.HttpContext.Response.ContentType = "image/Gif";
            FPContext.HttpContext.Response.Body.WriteAsync(bytes);
        }
    }
}
